/**
 * @author Isaiah Bullard
 */
package allObjects;

import org.junit.jupiter.api.*;

import java.sql.Connection;
import java.sql.SQLException;

public class GradebookTest {
    @Test
    void test(){
        try{
            String roster = School.findClass("Bobby T Bullard");
            if(roster != null){
                assert(true);
            }
            else{
                assert(false);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            assert(true);
        }
    }

    @Test
    void test2(){

        Connection c = School.getDBConnection();
        if(c == null){
            assert(false);
        }
        else{
            assert(true);
        }
    }

    @Test
    void test3(){
        try {
            School.createSchoolDb();
        } catch (SQLException e) {
            if(e.getMessage().equals("Table/View 'SCHOOL' already exists in Schema 'APP'.")){
                assert(true);
            }
            else{
                assert(false);
            }
        }
    }
}
