/**
 * @author Isaiah Bullard
 */
package allObjects;

import java.util.List;

public class Student extends Person {
    private Long studentID;
    private List<Class> classes;

    /**
     * retrieves ID
     * @return studentID
     */
    public Long getID() {
        return studentID;
    }

    /**
     * sets ID
     * @param studentID
     */
    public void setID(Long studentID) {
        this.studentID = studentID;
    }

    /**
     * edits account
     */
    public void editAccount() {

    }
}
