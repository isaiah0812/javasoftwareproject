/**
 * @author Isaiah Bullard
 */
package allObjects;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.Scanner;

public class School {
    private static final String dbConnection = "jdbc:derby:ex1connect;";
    private static final String dbPassWord = "";
    private static final String dbUser = "";
    private static final String dbDriver = "org.apache.derby.jdbc.EmbeddedDriver";
    private static ServerSocket serverConnection;
    private static Socket accountConnection;
    private static Connection connection = null;
    private static PrintWriter output;
    private static BufferedReader input;
    private static Long teacherID = (long)100000, studentID = (long)200000;
    private static Integer classID = 3000;

    /**
     * opens up the server
     * @param argv
     * @throws SQLException
     */
    public static void main(String[] argv) throws SQLException {
        try {
            createSchoolDb();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try{
            serverConnection = new ServerSocket(1122);
            while(true){
                accountConnection = serverConnection.accept();
                output = new PrintWriter(accountConnection.getOutputStream(), true);
                input = new BufferedReader(new InputStreamReader(accountConnection.getInputStream()));

                String loginInfo = input.readLine();
                String[] info = loginInfo.split(",");
                if(verifyLogin(info[0], info[1])){
                    try {
                        output.println(sendAccount(info[0]));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    output.println("yellow box");
                }
                try{
                    while(!accountConnection.isClosed()){
                        String action = input.readLine();
                        info = action.split(",");
                        if(info[0].equals("newTeacher")){
                            try {
                                createTeacher(info[2], info[1], info[3].charAt(0), info[4]);
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }
                        if(info[0].equals("newStudent")){
                            try {
                                createStudent(info[2], info[1], info[3].charAt(0), info[4]);
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }
                        if(info[0].equals("deleteUser")){
                            try{
                                deleteUser(info[1]);
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }
                        if(info[0].equals("newClass")){
                            try{
                                createClass(info[1], Long.parseLong(info[2]));
                            } catch(SQLException e){
                                e.printStackTrace();
                            }
                        }
                        if(info[0].equals("viewGrades")){
                            output.println(findClass(info[1]));
                        }
                        if(info[0].equals("editGrades")){
                            System.out.println(info[1]);
                            editGrades(info[1], info[2]);
                            output.println(findClass(info[2]));
                        }
                    }
                } catch (IOException e1){

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * changes the grades in the .csv file
     * @param students
     * @param instructor
     * @throws SQLException
     */
    public static void editGrades(String students, String instructor) throws SQLException {
        Statement statement = null;
        String command = "SELECT * FROM CLASSES WHERE TEACHER = '" + instructor + "'";
        FileWriter fileWrite = null;
        try{
            connection = getDBConnection();
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(command);
            if(rs.next()){
                try {
                    fileWrite = new FileWriter(new File("./" + rs.getInt("CLASS_ID") + ".csv"));
                    String[] newGrades = students.split(" ");
                    for(String s: newGrades){
                        s.replaceAll(";", ",");
                        fileWrite.write(s);
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    /**
     * finds a class and returns the people in that class
     * @param instructor
     * @return roster
     * @throws SQLException
     */
    public static String findClass(String instructor) throws SQLException {
        Statement statement = null;
        String command = "SELECT * FROM CLASSES WHERE TEACHER = '" + instructor + "'";
        Scanner fileRead = null;
        try{
            connection = getDBConnection();
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(command);
            String roster = new String();
            if(rs.next()){
                try {
                    fileRead = new Scanner(new File("./" + rs.getInt("CLASS_ID") + ".csv"));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                while(fileRead.hasNext()){
                    String info = fileRead.next();
                    roster += (info + " ");
                }
            }
            return roster;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return null;
    }

    /**
     * creates the databases for the school
     * @throws SQLException
     */
    public static void createSchoolDb() throws SQLException{
        Statement statement = null;
        String command = "CREATE TABLE SCHOOL(" + "USER_ID BIGINT NOT NULL, "
                + "TYPE VARCHAR(7) NOT NULL, "
                + "LASTNAME VARCHAR(40) NOT NULL, "
                + "FIRSTNAME VARCHAR(40) NOT NULL, "
                + "MIDDLEINITIAL CHAR NOT NULL, "
                + "EMAIL VARCHAR(100) NOT NULL, "
                + "PASSWORD VARCHAR(8) NOT NULL, "
                + "PRIMARY KEY (USER_ID) " + ")";
        try {
            connection = getDBConnection();
            statement = connection.createStatement();
            System.out.println(command);
            // execute the SQL stetement
            statement.execute(command);
            System.out.println("Table \"School\" is created!");
            command = "INSERT INTO SCHOOL" + "(USER_ID, TYPE, LASTNAME, FIRSTNAME, MIDDLEINITIAL, EMAIL, PASSWORD) "
                    + "VALUES(732979, 'Admin', 'Bullard', 'Isaiah', 'S', 'Isaiah_Bullard@baylor.edu', 'password')";
            statement = connection.createStatement();
            System.out.println(command);
            statement.executeUpdate(command);
            System.out.println("Admin entered into School!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        try{
            command = "CREATE TABLE CLASSES(" +
                    "CLASS_ID INT NOT NULL, " +
                    "CLASS_NAME VARCHAR(40) NOT NULL, " +
                    "TEACHER VARCHAR(100) NOT NULL, PRIMARY KEY (CLASS_ID))";
            connection = getDBConnection();
            statement = connection.createStatement();
            System.out.println(command);
            // execute the SQL stetement
            statement.execute(command);
            System.out.println("Table \"Classes\" is created!");
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    /**
     * retrieves a connection for the database
     * @return connection
     */
    public static Connection getDBConnection(){
        try {
            System.out.println("first");
            java.lang.Class.forName(dbDriver);
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        try {
            System.out.println("second");
            connection = DriverManager.getConnection(dbConnection, dbUser, dbPassWord);
            return connection;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return connection;
    }

    /**
     * creates a new student in the database
     * @param firstName
     * @param lastName
     * @param middleInitial
     * @param email
     * @throws SQLException
     */
    public static void createStudent(String firstName, String lastName, char middleInitial, String email) throws SQLException {
        Statement statement = null;
        String command = null;
        try{
            connection = getDBConnection();
            command = "INSERT INTO SCHOOL" + "(USER_ID, TYPE, LASTNAME, FIRSTNAME, MIDDLEINITIAL, EMAIL, PASSWORD) "
                    + "VALUES(" + studentID + ", 'Student', '" + lastName + "', '" + firstName + "', '" + middleInitial
                    + "', '" + email + "', 'password')";
            studentID++;
            statement = connection.createStatement();
            statement.executeUpdate(command);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            command = "SELECT * FROM SCHOOL";
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(command);
            while(rs.next()){
                System.out.println(rs.getLong("USER_ID") + ", "
                        + rs.getString("TYPE") + ", "
                        + rs.getString("LASTNAME") + ", "
                        + rs.getString("FIRSTNAME") + ", "
                        + rs.getString("MIDDLEINITIAL") + ", "
                        + rs.getString("EMAIL") + ", "
                        + rs.getString("PASSWORD"));
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    /**
     * creates a new teacher in the database
     * @param firstName
     * @param lastName
     * @param middleInitial
     * @param email
     * @throws SQLException
     */
    public static void createTeacher(String firstName, String lastName, char middleInitial, String email)
            throws SQLException {
        Statement statement = null;
        String command = null;
        try{
            connection = getDBConnection();
            command = "INSERT INTO SCHOOL" + "(USER_ID, TYPE, LASTNAME, FIRSTNAME, MIDDLEINITIAL, EMAIL, PASSWORD) "
                    + "VALUES(" + teacherID + ", 'Teacher', '" + lastName + "', '" + firstName + "', '" + middleInitial
                    + "', '" + email + "', 'password')";
            teacherID++;
            statement = connection.createStatement();
            statement.executeUpdate(command);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            command = "SELECT * FROM SCHOOL";
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(command);
            while(rs.next()){
                System.out.println(rs.getLong("USER_ID") + ", "
                        + rs.getString("TYPE") + ", "
                        + rs.getString("LASTNAME") + ", "
                        + rs.getString("FIRSTNAME") + ", "
                        + rs.getString("MIDDLEINITIAL") + ", "
                        + rs.getString("EMAIL") + ", "
                        + rs.getString("PASSWORD"));
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    /**
     * creates a new class in the database
     * @param name
     * @param instructor
     * @throws SQLException
     */
    public static void createClass(String name, Long instructor) throws SQLException {
        Statement statement = null;
        String command = "SELECT LASTNAME, " +
                "FIRSTNAME, " +
                "MIDDLEINITIAL " +
                "FROM SCHOOL " +
                "WHERE USER_ID = " + instructor;
        String teacherName = null;
        try{
            connection = getDBConnection();
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(command);
            if(rs.next()){
                teacherName = rs.getString("FIRSTNAME")
                        + " " + rs.getString("MIDDLEINITIAL")
                        + " " + rs.getString("LASTNAME");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try{
            statement = connection.createStatement();
            command = "INSERT INTO CLASSES (CLASS_ID, CLASS_NAME, TEACHER) " +
                    "VALUES(" + classID + ", '" + name + "', '" + teacherName + "')";
            statement.executeUpdate(command);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            command = "SELECT * FROM SCHOOL";
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(command);
            while(rs.next()){
                System.out.println(rs.getLong("USER_ID") + ", "
                        + rs.getString("TYPE") + ", "
                        + rs.getString("LASTNAME") + ", "
                        + rs.getString("FIRSTNAME") + ", "
                        + rs.getString("MIDDLEINITIAL") + ", "
                        + rs.getString("EMAIL") + ", "
                        + rs.getString("PASSWORD"));
            }
            command = "SELECT * FROM CLASSES";
            statement = connection.createStatement();
            rs = statement.executeQuery(command);
            while(rs.next()){
                System.out.println(rs.getInt("CLASS_ID") + ", "
                        + rs.getString("CLASS_NAME") + ", "
                        + rs.getString("TEACHER"));
            }
            if (!statement.equals(null)) {
                statement.close();
            }
            if (!connection.equals(null)) {
                connection.close();
            }
        }
    }

    public void insertStudent(Student newStudent){

    }

    public void insertTeacher(Teacher newTeacher){

    }

    /**
     * deletes a user from the database
     * @param id
     * @throws SQLException
     */
    public static void deleteUser(String id) throws SQLException {
        Statement statement = null;
        String command = "DELETE FROM SCHOOL WHERE USER_ID = " + Long.parseLong(id);
        try {
            connection = getDBConnection();
            statement = connection.createStatement();
            statement.execute(command);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            command = "SELECT * FROM SCHOOL";
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(command);
            while(rs.next()){
                System.out.println(rs.getLong("USER_ID") + ", "
                        + rs.getString("TYPE") + ", "
                        + rs.getString("LASTNAME") + ", "
                        + rs.getString("FIRSTNAME") + ", "
                        + rs.getString("MIDDLEINITIAL") + ", "
                        + rs.getString("EMAIL") + ", "
                        + rs.getString("PASSWORD"));
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    /**
     * verifies the login of a user
     * @param user
     * @param pword
     * @return true or false
     * @throws SQLException
     */
    public static boolean verifyLogin(String user, String pword) throws SQLException {
        Statement statement = null;
        String command = "SELECT PASSWORD FROM SCHOOL WHERE USER_ID = " + Long.parseLong(user);
        try {
            connection = getDBConnection();
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(command);
            if(!rs.next()){
                output.println("Invalid Username, try again");
            }
            else{
                if(!rs.getString("PASSWORD").equals(pword)){
                    return false;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return true;
    }

    /**
     * prepares acoount for client socket by string
     * @param info
     * @return account
     * @throws SQLException
     */
    public static String sendAccount(String info) throws SQLException {
        Statement statement = null;
        String command = "SELECT TYPE, LASTNAME, FIRSTNAME, MIDDLEINITIAL FROM SCHOOL WHERE USER_ID = " + Long.parseLong(info);
        try {
            connection = getDBConnection();
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(command);
            if(!rs.next()){
                System.out.println("result set empty");
            }
            else{
                return rs.getString("TYPE")
                        + " " + rs.getString("LASTNAME")
                        + " " + rs.getString("FIRSTNAME")
                        + " " + rs.getString("MIDDLEINITIAL");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return null;
    }
}
