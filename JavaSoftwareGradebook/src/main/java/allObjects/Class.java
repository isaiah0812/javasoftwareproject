/**
 * @author Isaiah Bullard
 */
package allObjects;

import java.util.List;
import java.util.Map;

public class Class {
    private Teacher teacher;
    private Map<Student, Integer> roster;
    private List<String> assignmentNames;

    /**
     * retrieves teacher
     * @return teacher
     */
    public Teacher getTeacher() {
        return teacher;
    }

    /**
     * sets teacher
     * @param teacher
     */
    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    /**
     * retrieves roster
     * @return roster
     */
    public Map<Student, Integer> getRoster() {
        return roster;
    }

    /**
     * sets roster
     * @param roster
     */
    public void setRoster(Map<Student, Integer> roster) {
        this.roster = roster;
    }

    /**
     * retrieves assignmentNames
     * @return assignmentNames
     */
    public List<String> getAssignmentNames() {
        return assignmentNames;
    }

    /**
     * set assignmentNames
     * @param assignmentNames
     */
    public void setAssignmentNames(List<String> assignmentNames) {
        this.assignmentNames = assignmentNames;
    }

    /**
     * edits the grades in the class
     */
    public void editGrades(){

    }

    /**
     * adds a new student to the class roster
     * @param newKid
     */
    public void addStudent(Student newKid){

    }
}
