/**
 * @author Isaiah Bullard
 */
package allObjects;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Window implements ActionListener {
    private JFrame frame;
    private JLabel label, label2;
    private JPanel panel, testPanel, display;
    private JButton submitButton, newTeachButton, newStudentButton, deleteButton, classButton, gradesButton;
    private JTextField userID, newLName, newFName, newMI, newEmail, search, className, teacherID;
    private JPasswordField userPW;
    private Socket accountToServer = null;
    private PrintWriter output;
    private BufferedReader input;
    private Admin userAdmin;
    private Teacher userTeacher;
    private Student userStudent;
    JLabel[] nameLabel;
    JFormattedTextField[] grade;

    /**
     * constructor
     */
    public Window(){
        frame = new JFrame("Gradebook");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        display = new JPanel();
        display.setOpaque(true);
        display.setBackground(new Color(255, 255, 255));
        frame.setContentPane(display);

        frame.setSize(500, 700);
        frame.setLocation(400, 150);

        frame.pack();
        frame.setVisible(true);

        login();
    }

    /**
     * lays out display for a login
     */
    public void login(){
        label = new JLabel("ID Number");
        label2 = new JLabel("Password");
        panel = new JPanel();
        submitButton = new JButton("Submit");
        userID = new JTextField(6);
        userID.addActionListener(this);

        userPW = new JPasswordField(8);
        userPW.addActionListener(this);

        submitButton.addActionListener(this);
        submitButton.setActionCommand("Submit");

        panel.add(label);
        panel.add(userID);
        panel.add(label2);
        panel.add(userPW);
        panel.add(submitButton);
        frame.getContentPane().add(panel);

        frame.pack();
        frame.setVisible(true);
    }

    /**
     * determines the action that is taken in the UI
     * @param e
     */
    public void actionPerformed(ActionEvent e) {
        if("Submit".equals(e.getActionCommand())){
            try{
                accountToServer = new Socket(InetAddress.getLocalHost(), 1122);
                output = new PrintWriter(accountToServer.getOutputStream(), true);
                input = new BufferedReader(new InputStreamReader(accountToServer.getInputStream()));
                if(userID.getText() != null && userPW.getPassword() != null){
                    output.println(userID.getText() + "," + new String(userPW.getPassword()));
                }
                String[] result = input.readLine().split(" ");
                if(result[0].equals("Admin")){
                    userAdmin = Admin.getInstance();
                    if(userAdmin != null){
                        System.out.println("W's and only W's in this house");
                    }
                    userAdmin.setFirstName(result[2]);
                    userAdmin.setMiddleInitial(result[3].charAt(0));
                    userAdmin.setLastName(result[1]);
                    userAdmin.setUserSocket(accountToServer);
                    userAdmin.setID(Long.parseLong(userID.getText()));
                    revertAdmin();

                }
                else if(result[0].equals("Teacher")){
                    userTeacher = new Teacher();
                    if(userTeacher != null){
                        System.out.println("W's and only W's in this house");
                    }
                    userTeacher.setFirstName(result[2]);
                    userTeacher.setMiddleInitial(result[3].charAt(0));
                    userTeacher.setLastName(result[1]);
                    userTeacher.setUserSocket(accountToServer);
                    userTeacher.setID(Long.parseLong(userID.getText()));
                    revertTeacher();
                }
                else if(result[0].equals("Student")){
                    panel.removeAll();
                    panel.add(new JLabel("lol wut"));
                    panel.updateUI();
                }
            } catch (UnknownHostException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        if("New Teacher".equals(e.getActionCommand())){
            panel.removeAll();
            panel.add(new JLabel("Last Name: "));
            newLName = new JTextField(40);
            newLName.addActionListener(this);
            panel.add(newLName);
            panel.add(new JLabel("First Name: "));
            newFName = new JTextField(40);
            newFName.addActionListener(this);
            panel.add(newFName);
            panel.add(new JLabel("MI: "));
            newMI = new JTextField(1);
            newMI.addActionListener(this);
            panel.add(newMI);
            panel.add(new JLabel("Email: "));
            newEmail = new JTextField(40);
            newEmail.addActionListener(this);
            panel.add(newEmail);
            JButton submitTeacher = new JButton("Submit Teacher");
            submitTeacher.addActionListener(this);
            submitTeacher.setActionCommand("Submit Teacher");
            panel.add(submitTeacher);
            panel.updateUI();
        }
        if("New Student".equals(e.getActionCommand())){
            panel.removeAll();
            panel.add(new JLabel("Last Name: "));
            newLName = new JTextField(40);
            newLName.addActionListener(this);
            panel.add(newLName);
            panel.add(new JLabel("First Name: "));
            newFName = new JTextField(40);
            newFName.addActionListener(this);
            panel.add(newFName);
            panel.add(new JLabel("MI: "));
            newMI = new JTextField(1);
            newMI.addActionListener(this);
            panel.add(newMI);
            panel.add(new JLabel("Email: "));
            newEmail = new JTextField(40);
            newEmail.addActionListener(this);
            panel.add(newEmail);
            JButton submitStudent = new JButton("Submit Student");
            submitStudent.addActionListener(this);
            submitStudent.setActionCommand("Submit Student");
            panel.add(submitStudent);
            panel.updateUI();
        }
        if("Submit Teacher".equals(e.getActionCommand())){
            userAdmin.createTeacher(newLName.getText()
                    + "," + newFName.getText()
                    + "," + newMI.getText()
                    + "," + newEmail.getText(),
                    output);
            revertAdmin();
        }
        if("Submit Student".equals(e.getActionCommand())){
            userAdmin.createStudent(newLName.getText()
                            + "," + newFName.getText()
                            + "," + newMI.getText()
                            + "," + newEmail.getText(),
                    output);
            revertAdmin();
        }
        if("Delete User".equals(e.getActionCommand())){
            searchBar();
        }
        if("Submit Search".equals(e.getActionCommand())){
            userAdmin.deleteUser(search.getText(), output);
            revertAdmin();
        }
        if("Create Class".equals(e.getActionCommand())){
            panel.removeAll();
            panel.add(new JLabel("Class Name: "));
            className = new JTextField(40);
            className.addActionListener(this);
            panel.add(className);
            panel.add(new JLabel("Teacher ID Number: "));
            teacherID = new JTextField(6);
            teacherID.addActionListener(this);
            panel.add(teacherID);
            JButton submitClass = new JButton("Submit");
            submitClass.addActionListener(this);
            submitClass.setActionCommand("Submit Class");
            panel.add(submitClass);

            panel.updateUI();
        }
        if("Submit Class".equals(e.getActionCommand())){
            userAdmin.createClass(className.getText(), Long.parseLong(teacherID.getText()), output);
            revertAdmin();
        }
        if("Edit Grades".equals(e.getActionCommand())){
            userTeacher.viewGrades(panel, output);
            showGrades();
        }
        if("Change Grades".equals(e.getActionCommand())){
            userTeacher.changeGrades(grade, nameLabel, output);
            showGrades();
        }
        if("Home".equals(e.getActionCommand())){
            revertTeacher();
        }
    }

    /**
     * reverts the screen back to an admin home screen
     */
    private void revertAdmin(){
        panel.removeAll();
        panel.add(new JLabel("Welcome " + userAdmin.getName()));
        newTeachButton = new JButton("New Teacher");
        newTeachButton.addActionListener(this);
        newTeachButton.setActionCommand("New Teacher");
        panel.add(newTeachButton);
        newStudentButton = new JButton("New Student");
        newStudentButton.addActionListener(this);
        newStudentButton.setActionCommand("New Student");
        panel.add(newStudentButton);
        deleteButton = new JButton("Delete User");
        deleteButton.addActionListener(this);
        deleteButton.setActionCommand("Delete User");
        panel.add(deleteButton);
        classButton = new JButton("Create Class");
        classButton.addActionListener(this);
        classButton.setActionCommand("Create Class");
        panel.add(classButton);

        panel.updateUI();
    }

    /**
     * reverts screen back to a teacher home screen
     */
    private void revertTeacher(){
        panel.removeAll();
        panel.add(new JLabel("Welcome " + userTeacher.getName()));
        gradesButton = new JButton("View/Edit Grades");
        gradesButton.addActionListener(this);
        gradesButton.setActionCommand("Edit Grades");
        panel.add(gradesButton);
        panel.updateUI();
    }

    /**
     * creates a search bar to search for a user
     */
    private void searchBar(){
        panel.removeAll();
        panel.add(new JLabel("Enter the User ID (Teacher/Student/Admin)"));
        search = new JTextField();
        search.setColumns(6);
        search.addActionListener(this);
        panel.add(search);
        JButton submitSearch = new JButton("Submit");
        submitSearch.addActionListener(this);
        submitSearch.setActionCommand("Submit Search");
        panel.add(submitSearch);
        panel.updateUI();
    }

    /**
     * shows the grades of a class
     */
    private void showGrades(){
        try{
            panel.removeAll();
            String roster = input.readLine();
            String[] students = roster.split(" ");
            grade = new JFormattedTextField[students.length];
            nameLabel = new JLabel[students.length];
            for(int i = 0; i < students.length; i++){
                String[] current = students[i].split(",");
                System.out.println(current.length);
                nameLabel[i] = new JLabel(current[0] + ", " + current[1] + " " + current[2]);
                panel.add(nameLabel[i]);
                grade[i] = new JFormattedTextField(Integer.parseInt(current[3]));
                grade[i].addActionListener(this);
                panel.add(grade[i]);
            }
            JButton enterGrades = new JButton("Enter All");
            enterGrades.addActionListener(this);
            enterGrades.setActionCommand("Change Grades");
            panel.add(enterGrades);
            JButton home = new JButton("Cancel");
            home.addActionListener(this);
            home.setActionCommand("Home");
            panel.add(home);
            panel.updateUI();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}