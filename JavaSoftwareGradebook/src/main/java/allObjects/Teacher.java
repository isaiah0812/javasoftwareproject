/**
 * @author Isaiah Bullard
 */
package allObjects;

import javax.swing.*;
import java.io.PrintWriter;

public class Teacher extends Person {
    private Long teacherID;
    private Class course;

    /**
     * retrieves id
     * @return teacherID
     */
    public Long getID() {
        return teacherID;
    }

    /**
     * sets id
     * @param teacherID
     */
    public void setID(Long teacherID) {
        this.teacherID = teacherID;
    }

    /**
     * retrieves course
     * @return course
     */
    public Class getCourse() {
        return course;
    }

    /**
     * sets course
     * @param course
     */
    public void setCourse(Class course) {
        this.course = course;
    }

    /**
     * edits account
     */
    public void editAccount() {

    }

    /**
     * sends teacher to system to retrieve classes
     * @param panel
     * @param output
     */
    public void viewGrades(JPanel panel, PrintWriter output) {
        panel.removeAll();
        String[] lastAndFirst = getName().split(", ");
        String name = lastAndFirst[1] + " " + lastAndFirst[0];
        System.out.println(name);
        output.println("viewGrades," + name);

    }

    /**
     * alters the way the grade screen looks aftergrads change
     * @param grades
     * @param names
     * @param output
     */
    public void changeGrades(JFormattedTextField[] grades, JLabel[] names, PrintWriter output){
        String param = new String();
        for(int i = 0; i < grades.length; i++){
            String info = new String();
            String[] lastAndFirst = names[i].getText().split(", ");
            info += lastAndFirst[0] + ";";
            System.out.println(info);
            String[] firstAndMI= lastAndFirst[1].split(" ");
            info += firstAndMI[0] + ";" + firstAndMI[1] + ";" + grades[i].getText();
            System.out.println(info);
            param += info + " ";
            System.out.println(param);
        }
        param = param.substring(0, param.length()-1);
        output.println("editGrades," + param + "," + getName());
    }
}
