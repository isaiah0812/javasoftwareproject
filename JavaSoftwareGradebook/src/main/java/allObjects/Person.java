/**
 * @author Isaiah Bullard
 */
package allObjects;

import java.net.Socket;

public abstract class Person {
    private String firstName;
    private String lastName;
    private char middleInitial;
    private String email;
    private String pword;
    private Socket userSocket = null;

    /**
     * retrieves name of person
     *
     * @return name
     */
    public String getName(){
        return lastName + ", " + firstName + " " + middleInitial;
    }

    /**
     * sets first name
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * sets last name
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * sets middle initial
     * @param middleInitial
     */
    public void setMiddleInitial(char middleInitial) {
        this.middleInitial = middleInitial;
    }

    /**
     * retrieves email
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * sets email
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * retrieves password
     * @return pword
     */
    public String getPword() {
        return pword;
    }

    /**
     * sets password
     * @param pword
     */
    public void setPword(String pword) {
        this.pword = pword;
    }

    /**
     * retrieves userSocket
     *
     * @return userSocket
     */
    public Socket getUserSocket() {
        return userSocket;
    }

    /**
     * sets socket
     * @param userSocket
     */
    public void setUserSocket(Socket userSocket) {
        this.userSocket = userSocket;
    }

    abstract public void editAccount();
}
