/**
 * @author Isaiah Bullard
 */
package allObjects;

import java.io.PrintWriter;

public class Admin extends Person{
    private Long adminID;
    private static Admin instance = null;

    /**
     * singleton generator of instance
     * @return instance
     */
    public static Admin getInstance(){
        if(instance == null){
            instance = new Admin();
        }
        return instance;
    }

    /**
     * retrive id
     * @return adminID
     */
    public Long getID() {
        return adminID;
    }

    /**
     * sets id
     * @param adminID
     */
    public void setID(Long adminID) {
        this.adminID = adminID;
    }

    /**
     * edits account
     */
    public void editAccount() {

    }

    /**
     * creates a new class
     * @param name
     * @param id
     * @param output
     */
    public void createClass(String name, Long id, PrintWriter output){
        output.println("newClass," + name + "," + id);
    }

    /**
     * creates a new teacher account
     * @param info
     * @param output
     */
    public void createTeacher(String info, PrintWriter output){
        output.println("newTeacher," + info);
    }

    /**
     * creates a new student account
     * @param info
     * @param output
     */
    public void createStudent(String info, PrintWriter output){
        output.println("newStudent," + info);
    }

    /**
     * edits a student account
     */
    public void editStudent(){

    }

    /**
     * edits a teacher account
     */
    public void editTeacher(){

    }

    /**
     * deletes a user's account
     * @param s
     * @param output
     */
    public void deleteUser(String s, PrintWriter output){
        output.println("deleteUser," + s);
    }
}
